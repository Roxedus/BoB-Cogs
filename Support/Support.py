from discord.ext import commands
import discord
from cogs.utils import checks
from __main__ import send_cmd_help
from .utils.dataIO import dataIO, fileIO
import os

default_settings = {}

class Support:

    def __init__(self, bot):
        self.bot = bot
        self.qa_file = "data/support/faq.json"
        self.qa = fileIO(self.qa_file, "load")

    @commands.command(name="f12", pass_context=True)
    async def f12(self, ctx, member: discord.Member = None, tab: str = None):
        """
        tab is the tab-name. like console, network
        """

        base = "Press the `F12` button on your keyboard to open the browser-console. Switch to the `{0}` tab." \
               "\nPaste any errors you find"

        if tab is not None:
            msg = base.format(tab)
        else:
            msg = base.format("console")


        await self.bot.delete_message(ctx.message)
        if member is None:
            await self.bot.send_message(ctx.message.channel, "{0}".format(msg))
        if member is not None:
            await self.bot.send_message(ctx.message.channel, "{0}\n{1}".format(member.mention, msg))



    @commands.command(name="faq", pass_context=True)
    async def faq(self, ctx, *, search):
        """
        \n
        Take extra argumets to search the FAQ database
        """

        message = ctx.message
        author = message.author
        channel = message.channel
        server = message.server.id
        search_dict = search.split(" ")
        acc = 0
        try:
            for id in self.qa[server]:
                for e in search_dict:
                    d = e.lower()
                    if d in self.qa[server][id]["q"].lower():
                        acc += 1
                        if acc == len(search_dict):

                            title = "Here is your answer:"
                            q = "{0}?".format(self.qa[server][id]["q"])
                            a = self.qa[server][id]["a"]
                            foot = "Summoned by {0}".format(author)

                            embed = discord.Embed(title=title, colour=author.colour)
                            embed.add_field(name=q, value=a)
                            embed.set_footer(text=foot, icon_url=author.avatar_url)
                            await self.bot.say(embed=embed)
                            raise ValueError("yeet")

        except ValueError as e:
            pass
        except:
            await self.bot.send_message(channel, "No result found for {0}".format(search))

    @commands.group(name="faq_conf", pass_context=True, no_pm=True, invoke_without_command=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def faq_conf(self, ctx):
        """
        Yes.
        """
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)


    @faq_conf.command(name="add", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def faq_faq_add(self, ctx, quest, answ):
        try:
            if ctx.message.server.id in self.qa:
                pass
            elif ctx.message.server.id not in self.qa:
                await self.bot.send_message(ctx.message.channel, "ServerID does not exist, creating")
                self.qa[ctx.message.server.id] = {}
                self.qa[ctx.message.server.id]["1"] =  {
            "a" : "PLACEHOLDERERESFS",
            "q" : "PLACHOLDERWAWDA"
        }
                fileIO(self.qa_file, "save", self.qa)
            else:
                raise ValueError("Big Fucking Error")

            new_id = await self.nid(ctx)
            self.qa[ctx.message.server.id][new_id] = {}
            self.qa[ctx.message.server.id][new_id]["q"] = quest
            self.qa[ctx.message.server.id][new_id]["a"] = answ
            fileIO(self.qa_file, "save", self.qa)
            await self.bot.send_message(ctx.message.channel, "Added: ```Q: {0}\nA: {1}```".format(quest,answ))


        except ValueError as e:
            print("wat")


    @faq_conf.command(name="del", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def faq_conf_del(self, ctx, i):
        try:
            del self.qa[ctx.message.server.id][i]
            fileIO(self.qa_file, "save", self.qa)
            await self.bot.send_message(ctx.message.channel, "Message Deleted")
        except:
            await self.bot.send_message(ctx.message.channel, "Message not Deleted")


    @faq_conf.command(name="edit", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def faq_conf_edit(self, ctx, i, q, a):
        try:
            self.qa[ctx.message.server.id][i] = {}
            self.qa[ctx.message.server.id][i]["q"] = q
            self.qa[ctx.message.server.id][i]["a"] = a
            fileIO(self.qa_file, "save", self.qa)
            await self.bot.send_message(ctx.message.channel, "Message Edited")
        except:
            await self.bot.send_message(ctx.message.channel, "Message not Edited")


    @faq_conf.command(name="list", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def faq_conf_list(self, ctx):
        msg = ""
        for d in self.qa[ctx.message.server.id]:
            try:
                    print(d)
                    a = self.qa[ctx.message.server.id][d]["a"]
                    q = self.qa[ctx.message.server.id][d]["q"]
                    msg += "ID {0}\n```Q: {1}\nA: {2}```\n".format(d, q, a)
            except:
                pass
            await self.bot.send_message(ctx.message.channel, msg)

    @faq_conf.command(name="id", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def faq_faq_id(self, ctx):
        await self.bot.send_message(ctx.message.channel, await self.nid(ctx))

    async def nid(self, ctx):
        ya = fileIO(self.qa_file, "load")
        _id = "0"
        high = "0"
        for i in ya[ctx.message.server.id]:
            if i > _id:
                _id = i
                high = "{}".format(int(_id) + 1)
        return high



def check_folders():
    if not os.path.exists("data/support"):
        print("Creating data/support folder...")
        os.makedirs("data/support")


def check_files_data():
    f = "data/support/faq.json"
    if not fileIO(f, "check"):
        print("Creating support faq.json...")
        fileIO(f, "save", default_settings)
    else: #consistency check
        current = fileIO(f, "load")
        for k,v in current.items():
            if v.keys() != {}:
                for key in default_settings.keys():
                    if key not in v.keys():
                        current[k][key] = default_settings[key]
                        print("Adding " + str(key) + " field to support faq.json")
        fileIO(f, "save", current)


def setup(bot):
    check_folders()
    check_files_data()
    n = Support(bot)
    bot.add_cog(n)