import os
from discord.ext import commands
from cogs.utils import checks
from .utils.dataIO import fileIO
from __main__ import send_cmd_help

default_settings = {}
default_messages = {}




class RepeatAfterMe:
    """DEMOTEXT"""

    def __init__(self, bot):
        self.bot = bot
        self.settings = fileIO("data/RepeatAfterMe/data.json", "load")
        self.defined_msg = fileIO("data/RepeatAfterMe/definded_msg.json", "load")

    @commands.group(name="rem_conf", pass_context=True, no_pm=True, invoke_without_command=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def rem_conf(self, ctx):
        """demotext"""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)

    @rem_conf.command(name="list", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def rem_conf_list(self, ctx):
        msg = ""
        for def_msg_id in self.defined_msg:
            content = self.defined_msg[def_msg_id]["Content"]
            msg += "ID {}\n".format(def_msg_id)
            msg += "```{}```\n".format(str(content)[0:200])
        await self.bot.send_message(ctx.message.channel, msg)

    @rem_conf.command(name="show", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def rem_conf_show(self, ctx, ID):
        msg = self.defined_msg[ID]["Content"]
        await self.bot.send_message(ctx.message.channel, msg)

    @rem_conf.command(name="add", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def rem_conf_add(self, ctx, *, msg):
        msg_ = str(msg)
        ID = "0"
        for def_msg_id in self.defined_msg:
            ID = "{}".format(int(def_msg_id) + 1)
            print(ID)

        msg_id = ID
        self.defined_msg[msg_id] = {}
        self.defined_msg[msg_id]["Content"] = msg_
        fileIO("data/RepeatAfterMe/definded_msg.json", "save", self.defined_msg)
        await self.bot.send_message(ctx.message.channel, "Added message: ```\n{0}\n``` as ID:{1}".format(msg_, ID))

    @rem_conf.command(name="del", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def rem_conf_del(self, ctx, ID):
        try:
            del self.defined_msg[ID]
            fileIO("data/RepeatAfterMe/definded_msg.json", "save", self.defined_msg)
            await self.bot.send_message(ctx.message.channel, "Message Deleted")
        except:
            await self.bot.send_message(ctx.message.channel, "Message not Deleted")

    @rem_conf.command(name="update", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def rem_conf_update(self, ctx, ID, *, new_msg):
        for def_msg_id in self.defined_msg:
            if ID is def_msg_id:
                self.defined_msg[ID] = {}
                self.defined_msg[ID]["Content"] = new_msg
                fileIO("data/RepeatAfterMe/definded_msg.json", "save", self.defined_msg)
                await self.bot.send_message(ctx.message.channel, "Edited: ```\n{0}\n``` as ID:{1}".format(new_msg, ID))



    @commands.group(name="rem", pass_context=True, no_pm=True, invoke_without_command=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def rem(self, ctx):
        """demotext"""
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)

    @rem.command(name="say", pass_context=True, no_pm=True)
    async def rem_say(self, ctx, *, msg):
        send = await self.bot.send_message(ctx.message.channel, msg)

        self.settings[send.id] = default_settings
        self.settings[send.id]["ChannelID"] = ctx.message.channel.id
        self.settings[send.id]["ContentID"] = "customized"
        fileIO("data/RepeatAfterMe/data.json", "save", self.settings)

    @rem.command(name="spawn", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def rem_spawn(self, ctx, ID, ChannelID):
        try:
            for msg_id in self.defined_msg:
                if ID is not msg_id:
                    pass
                if ID is msg_id:
                    channel = self.bot.get_channel(ChannelID)
                    msg = self.defined_msg[ID]["Content"]
                    send = await self.bot.send_message(channel, msg)

                    self.settings[send.id] = default_settings
                    self.settings[send.id]["ChannelID"] = ChannelID
                    self.settings[send.id]["ContentID"] = ID
                    fileIO("data/RepeatAfterMe/data.json", "save", self.settings)
                else:
                    pass
        except:
            await self.bot.send_message(ctx.message.channel, "Something went wrong, are you sure this ID exsists?")

    @rem.command(name="update", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def rem_update(self, ctx, ID, MessageID):
        msg = self.defined_msg[ID]["Content"]
        channel = self.bot.get_channel(self.settings[MessageID]["ChannelID"])
        message_obj = await self.bot.get_message(channel, str(MessageID))
        await self.bot.edit_message(message=message_obj, new_content=msg)
        await self.bot.send_message(ctx.message.channel, "Message #{0} edited in {1}".format(MessageID, channel.mention))

    @rem.command(name="edit", pass_context=True, no_pm=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def rem_edit(self, ctx, MessageID, *, msg):
        channel = self.bot.get_channel(self.settings[MessageID]["ChannelID"])
        message_obj = await self.bot.get_message(channel, str(MessageID))
        await self.bot.edit_message(message=message_obj, new_content=msg)
        await self.bot.send_message(ctx.message.channel, "Message #{0} edited in {1}".format(MessageID, channel.mention))


def check_folders():
    if not os.path.exists("data/RepeatAfterMe"):
        print("Creating data/RepeatAfterMe folder...")
        os.makedirs("data/RepeatAfterMe")


def check_files_data():
    f = "data/RepeatAfterMe/data.json"
    if not fileIO(f, "check"):
        print("Creating RepeatAfterMe data.json...")
        fileIO(f, "save", default_settings)
    else: #consistency check
        current = fileIO(f, "load")
        for k,v in current.items():
            if v.keys() != {}:
                for key in default_settings.keys():
                    if key not in v.keys():
                        current[k][key] = default_settings[key]
                        print("Adding " + str(key) + " field to RepeatAfterMe data.json")
        fileIO(f, "save", current)

def check_files_msg():
    f = "data/RepeatAfterMe/definded_msg.json"
    if not fileIO(f, "check"):
        print("Creating RepeatAfterMe definded_msg.json...")
        fileIO(f, "save", default_messages)
    else: #consistency check
        current = fileIO(f, "load")
        for k,v in current.items():
            if v.keys() != default_messages.keys():
                for key in default_messages.keys():
                    if key not in v.keys():
                        current[k][key] = default_messages[key]
                        print("Adding " + str(key) + " field to RepeatAfterMe definded_msg.json")
        fileIO(f, "save", current)

def setup(bot):
    check_folders()
    check_files_data()
    check_files_msg()
    n = RepeatAfterMe(bot)
    bot.add_cog(n)
